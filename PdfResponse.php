<?php
/**
 * Created by PhpStorm.
 * User: winus
 * Date: 05-12-16
 * Time: 18:10
 */

namespace tdsc\wkhtmltopdf;


use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class PdfResponse extends Response
{
    public function __construct($content, $status = 200, array $headers = array())
    {
        parent::__construct($content, $status, $headers);

    }

    public function sendHeaders()
    {
        $this->headers->set('Content-Type', 'application/pdf');
        $this->headers->set('Content-Length', strlen($this->getContent()));


        $disposition = $this->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, 'pdf.pdf');
        $this->headers->set('Content-Disposition', $disposition);
        return parent::sendHeaders(); // TODO: Change the autogenerated stub
    }
}