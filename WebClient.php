<?php
/**
 * Created by PhpStorm.
 * User: winus
 * Date: 12-10-16
 * Time: 08:59
 */

namespace tdsc\wkhtmltopdf;


class WebClient
{
    public static function fetch($url){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_COOKIE, session_name() . '=' . session_id());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        session_write_close();
        $content = curl_exec($ch);
        @session_start();
        curl_close($ch);

        return $content;
    }
}